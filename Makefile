PHP?=/usr/bin/php
PERL?=/usr/bin/perl
LOGGER?=/usr/bin/logger
GREP?=/bin/grep
SED?=/bin/sed
ECHO?=/bin/echo
CUT?=/usr/bin/cut
SEQ?=/usr/bin/seq
WGET?=/usr/bin/wget
TAR?=/bin/tar

DESCRIPTION=

all: list.task

include $(wildcard tasks/*.mk)
include $(wildcard vendor/*/tasks/*.mk)
include $(wildcard vendor/*/*/tasks/*.mk)

DESCRIPTION+="list.task: Available Tasks"
list.task:
	@$(ECHO) "Tasks List: (APP_ENVIRONMENT=$(APP_ENVIRONMENT))" && (for TASK in $(DESCRIPTION); do echo "." "$$TASK"; done) | sort -t: --key=1,1

.PHONY: help.%
help.%:
	@(for LINE in $(HELP_$*); do $(ECHO) $$LINE; done)
