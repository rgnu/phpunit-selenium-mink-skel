<?php

use Behat\Mink\Mink;

abstract class MinkTestCase extends PHPUnit_Framework_TestCase
{
    private static $mink;

    public static function setUpBeforeClass()
    {
        if (null === self::$mink) {
            self::$mink = new Mink();
        }
    }

    protected function teardown()
    {
        $this->getMink()->resetSessions();
    }

    protected function getMink()
    {
        return self::$mink;
    }

    protected function getSession($name = null)
    {
        return $this->getMink()->getSession($name);
    }

    protected function getPage($name = null)
    {
        return $this->getSession($name)->getPage();
    }

    protected function assertSession($name = null)
    {
        return $this->getMink()->assertSession($name);
    }
}