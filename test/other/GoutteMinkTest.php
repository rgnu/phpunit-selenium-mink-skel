<?php

use Behat\Mink\Mink,
    Behat\Mink\Session,
    Behat\Mink\Driver\GoutteDriver,
    Behat\Mink\Driver\Goutte\Client as GoutteClient;

class GoutteMinkTest extends MinkTestCase
{
    protected function setUp()
    {
        if (!$this->getMink()->hasSession('goutte')) {
            $this->getMink()->registerSession('goutte', new Session(new GoutteDriver(new GoutteClient())));
            $this->getMink()->setDefaultSessionName('goutte');
        }
    }

    public function testHome()
    {
        $this->getSession()->visit('http://www.olx.com.ar/');
        $this->assertEquals(
            'Anuncios gratis en Argentina, anuncios clasificados en Argentina (Compra - Venta en Argentina, Motor en Argentina, Viviendas - Locales en Argentina, Comunidad en Argentina,...)', 
            $this->getPage()->find('css', 'title')->getText()
        );
    }
}

