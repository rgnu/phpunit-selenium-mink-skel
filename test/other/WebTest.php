<?php

class WebTest extends PHPUnit_Extensions_SeleniumTestCase
{
    protected function setUp()
    {
        $this->setBrowser('*firefox');
        $this->setBrowserUrl('file:///');
    }
 
    public function testHome()
    {
        $this->open('http://www.olx.com/');
        $this->assertTitle('Classifieds, Free Classifieds, Online Classifieds | OLX.com');
        $this->assertElementPresent('xpath=//a[@href="http://www.olxalgerie.com/" and @rel="nofollow"]');
        $this->assertElementPresent('xpath=//a[@href="http://www.olx.com.ar/" and not(@rel="nofollow")]');
    }

    public function testHome2()
    {
        $this->open('http://www.olx.com.ar/');
        $this->assertTitle('Classifieds, Free Classifieds, Online Classifieds | OLX.com');
        $this->assertElementPresent('xpath=//a[@href="http://www.olxalgerie.com/" and @rel="nofollow"]');
        $this->assertElementPresent('xpath=//a[@href="http://www.olx.com.ar/" and not(@rel="nofollow")]');
    }
}
?>
