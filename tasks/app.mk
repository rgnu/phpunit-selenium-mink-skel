DESCRIPTION+="app.build: Build application"
.PHONY: app.build

app.build: app.build.pre app.build.post

.PHONY: app.build.pre

app.build.pre:
	rm -rf $(APP_BUILD_DIR) && mkdir -p $(APP_BUILD_DIR)
	$(MAKE) composer.update


.PHONY: app.build.post

app.build.post: app.build.pre
	$(MAKE) test checkstyle pmd


DESCRIPTION+="app.deploy: Deploy App to production"
.PHONY: app.deploy 

app.deploy: composer.install


.PHONY: composer.update
DESCRIPTION+="composer.update: Run composer update"
composer.update: composer.download
	$(PHP) $(APP_BIN_DIR)/composer.phar update


.PHONY: composer.download
DESCRIPTION+="composer.download: Download composer"
composer.download:
	mkdir -p $(APP_BIN_DIR) \
	&& ([ -e $(COMPOSER) ] || $(WGET) -q -O- http://getcomposer.org/installer | $(PHP) -- --install-dir=$(APP_BIN_DIR))

DESCRIPTION+="composer.install: Run composer install"
.PHONY: composer.install

composer.install: composer.download
	$(PHP) $(COMPOSER) install


