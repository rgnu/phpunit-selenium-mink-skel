APP_ENVIRONMENT?=production
APP_DIR?=$(CURDIR)
APP_VENDOR_DIR?=$(APP_DIR)/vendor
APP_BIN_DIR?=$(APP_VENDOR_DIR)/bin
APP_BUILD_DIR?=$(APP_DIR)/build

include $(wildcard $(APP_DIR)/config/environment/$(APP_ENVIRONMENT)/tasks/*.mk)

PHPUNIT?=$(APP_VENDOR_DIR)/bin/phpunit

COMPOSER?=$(APP_VENDOR_DIR)/bin/composer.phar
