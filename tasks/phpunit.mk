PHPUNIT?=$(APP_BIN_DIR)/phpunit
PHPUNIT_MAXMEMSIZE?=64M

# Definicion de parametrizacion pre-reglas

ifdef COVERAGE_DIR
	PHPUNIT_ARGS+= --coverage-html $(COVERAGE_DIR)
endif

ifdef FILTER
	PHPUNIT_ARGS+= --filter $(FILTER)
endif

ifdef COLORS
	PHPUNIT_ARGS+= --colors
endif

ifdef VERBOSE
	PHPUNIT_ARGS+= --verbose
endif

ifdef LIST_GROUPS
	PHPUNIT_ARGS+= --list-groups
endif

ifdef EXCLUDE_GROUPS
	PHPUNIT_ARGS+= --exclude-group $(EXCLUDE_GROUPS)
endif

ifdef GROUP
	PHPUNIT_ARGS+= --group $(GROUP)
endif

ifdef CONFIGURATION
	PHPUNIT_ARGS+= --configuration $(CONFIGURATION)
endif

# Definicion de reglas

.PHONY: test
DESCRIPTION+="test: Make All Tests"
test:
	$(PHPUNIT) $(PHPUNIT_ARGS)
