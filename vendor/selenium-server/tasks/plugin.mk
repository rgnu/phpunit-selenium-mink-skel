SELENIUM-SERVER?=$(APP_VENDOR_DIR)/selenium-server/bin/selenium-server

.PHONY: selenium-server
DESCRIPTION+="selenium-server: Run Selenium Server"
selenium-server:
	$(SELENIUM-SERVER)
